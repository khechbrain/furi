import React from 'react';

const Navbar = () => {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light px-5 py-3">
                <a className="navbar-brand fs-1" href="#">Furi</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse px-5" id="navbarNav">
                    <ul className="navbar-nav ms-auto">
                    <li className="nav-item active">
                        <a className="nav-link fs-6 text-dark mx-2" href="#">About Us</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link fs-6 text-dark mx-2" href="#">Catalog</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link fs-6 text-dark mx-2" href="#">Projects</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link fs-6 text-dark mx-2" href="#">Blog</a>
                    </li>
                    <li className="nav-item">
                        <a className="nav-link fs-6 text-dark mx-2" href="#">Contacts</a>
                    </li>
                    </ul>
                </div>
            </nav>
        </div>
    );
};

export default Navbar;